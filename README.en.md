# WaveApiApplication

#### Description
是一个基于oracle 数据 库的轻量级接口管理平台，主要是对一些pl/sql开发人员，不熟悉java开发语言，不了解restful 接口的开发技术的人，只需要写plsql语句就可以让外围系统用restful风格的形式访问oracle数据，同时又能向oracle 数据 库提供数据 ，让plsql开发人员只需要专注业务实现 ，不需要关注技术实现。 
本软件可以提供功能如下： 
1）可以向外提供数据 ，plsql开发人员，只需要写相关的视图或者把数据 写到表里就可以了 
2）可以让让外围系统向oralce 数据 库传数据， 
3）本系统可以替代 Oracle Erp SOA 模块的功能，也可以用在非oracle erp系统里，本系统可以独立一个应用使用 
4）本系统支持数据加密，可以对其中的一些接口是否采用数据 加密，一些接口不采用加密，加密方式采用RSA 1024 加密的一种PEM 证书。 
5）可以免费使用3个月，满意之后收费

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
