# WaveApiApplication

#### 介绍
是一个基于oracle 数据库的轻量级接口管理平台，主要是对一些pl/sql开发人员，不熟悉java开发语言，不了解restful 接口的开发技术的人，只需要写plsql语句就可以让外围系统用restful风格的形式访问oracle数据，同时又能向oracle 数据 库提供数据 ，让plsql开发人员只需要专注业务实现 ，不需要关注技术实现。 

本软件可以提供功能如下： 
1.  可以向外提供数据 ，plsql开发人员，只需要写相关的视图或者把数据 写到表里就可以了 
2.  可以让让外围系统向oralce 数据 库传数据， 
3.  本系统可以替代 Oracle Erp SOA 模块的功能，也可以用在非oracle erp系统里，本系统可以独立一个应用使用 
4.  本系统支持数据加密，可以对其中的一些接口是否采用数据 加密，一些接口不采用加密，加密方式采用RSA 1024 加密的一种PEM 证书。 
5.  可以试用，满意之后收费

本项目的目标是让数据库管理人员 0 JAVA 代码 来实现restful 接口开发。

#### 软件架构
软件架构后台主架构采用了spring boot 技术和 数据层采用mybatis 技术，日志采用log4j,前台采用了vue技术 和表示层用了iview技术,前后台数据传递采用alibaba fastjson.


#### 安装教程

1.   在数据库执行sql目录的文件
2.  把certificatepolicy目录的所有证书拷贝到服务器目录
3.  修改jar/WeApiFushion-0.0.1-SNAPSHOT.jar 中的BOOT-INF/classes/applicaiton-dev.yml 
    里面的数据库链接，证书目录指向自己的目录

#### 使用说明

1.  安装之后，可以运行 jar/startWeApi.sh 文件
2.  前台程序，请联系微信284666889
3.  本程序提供java用例：jar/TestExample.java
4.  rsa_private_key_pkcs8.pem 数据通信加密私钥证书；rsa_public_key.pem 数据通信加密公钥证书，rsa_soft_public_key.pem 软件授权证书
5.  soft_information.we 软件授权信息，user_list.we 接口用户信息。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
