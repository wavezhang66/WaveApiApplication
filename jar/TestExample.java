package com.ws.controller;


        import com.alibaba.fastjson.JSON;
        import com.alibaba.fastjson.JSONObject;
        import com.ws.Util.RsaMain;
        import com.ws.bean.Ws_Data_Serializable;
        import org.apache.cxf.endpoint.Client;
        import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;


        import java.io.BufferedReader;
        import java.io.InputStreamReader;
        import java.io.OutputStream;
        import java.net.HttpURLConnection;
        import java.net.URL;
        import java.net.URLEncoder;
        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;
public class TestExample
{

    public static void main(String[] args) {

        TestExample testFunction=new TestExample();
        //testFunction.soaSendData();
        testFunction.restfulSendData();//发送数据
        //testFunction.restfulGetData();//获得数据
        //testFunction.restfulRsaGetPkgData();
       // testFunction.restfulRsaSendData();//发送数据
        //testFunction.restfulRsaGetData();//获得数据


    }

    public void restfulGetData() {
        String url="http://127.0.0.1:6110/interface/restful/getInforPageDatalistApi";
        //String url="http://192.168.0.107:6110/interface/restful/getInforPageDatalist";
        String param="requestContent=";

        JSONObject obj = new JSONObject();
        obj.put("signInformation", "N2VXOE1VVGQwVWlrQVpxc3l1bFhFTE8zNThDRktWbDhMNkRGZFRKK2FRNEJaV1Q5bEZXekIxK0hy\n" +
                "VHF4ZEVOZytSaXNkcnpwMy9XRQpMWHhMdnJLMTVXcXhVVFhVb3F4alRxeElTUlYwblJmRkZML1Bi\n" +
                "d2duQk8yb0JHai9SOTdYK2QwR08xV0gyVUFsUGE5TGdWN2tXNjBRClE4Um9Mb2xhWFFweWxSUkFE\n" +
                "a0E9");
        obj.put("businessTypeCode","BASE_ITEM_DATA");
        obj.put("pageIndex","1");
        obj.put("sqlWhere","INTF_ID=93 and ITEM_DESC like '%--%'");



        try {
            param = param + URLEncoder.encode(obj.toString(), "UTF-8");


            String resultString = postRequest(url, param);
            System.out.println("resultString:" + resultString);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    //不加密传送
    public void restfulSendData() {
        String url="http://127.0.0.1:6110/interface/restful/postInforDataApi";
        //String url="http://192.168.0.107:6110/interface/restful/getInforPageDatalist";
        String param="requestContent=";

		/*JSONObject obj = new JSONObject();
		obj.put("signInformation", "ZytWZ09KN0xYTHMrWHVmSzQwbTRWSmFLSFRmb1dIUzdlWFlaNGxKaThrZGxtaUhvUXJZZGdZc2Ns\n" +
				"TjYyOUs5Njc4a2Nyc0FPUkV5QgpNNHJhV3VaUlh3ZUUveTVaK3h0ckNWSXAvbFZqZzFuQy9EdVFE\n" +
				"VWNpV2tBSkVLcXZxcGxZYUN2UUxWWE04cy9mSm9zTWMwWGFGRlJ3CmZZNG5DVUMzOENnRXo1OHor\n" +
				"b1U9");
		obj.put("businessTypeCode","BASE_ITEM_DATA");
		obj.put("pageIndex","1");
		obj.put("sqlWhere","INTF_ID=93 and ITEM_DESC like '%--%'");*/

        List<Map<String,Object>> list = new ArrayList<>();
        Map<String,Object> listMap1 = new HashMap<>();
        listMap1.put("ITEM_CODE","王强vvvv");
        listMap1.put("ITEM_DESC","王强1111-----");
        listMap1.put("ITEM_TYPE","王强TYPE");
        listMap1.put("CUX_SOURCE_ID","2");
        list.add(listMap1);

        Map<String,Object> listMap2 = new HashMap<>();
        listMap2.put("ITEM_CODE","余业vvvvvvy");
        listMap2.put("ITEM_DESC","余业2222-----");
        listMap2.put("ITEM_TYPE","王强TYPE");
        listMap2.put("CUX_SOURCE_ID","1");
        list.add(listMap2);

        Map<String,Object> listMap3 = new HashMap<>();
        listMap3.put("ITEM_CODE","张领vvvvvy");
        listMap3.put("ITEM_DESC","张领3333-----");
        listMap3.put("ITEM_TYPE","王强TYPE");
        listMap3.put("CUX_SOURCE_ID","3");
        list.add(listMap3);
        JSONObject obj = new JSONObject();
        obj.put("signInformation", "N2VXOE1VVGQwVWlrQVpxc3l1bFhFTE8zNThDRktWbDhMNkRGZFRKK2FRNEJaV1Q5bEZXekIxK0hy\n" +
                "VHF4ZEVOZytSaXNkcnpwMy9XRQpMWHhMdnJLMTVXcXhVVFhVb3F4alRxeElTUlYwblJmRkZML1Bi\n" +
                "d2duQk8yb0JHai9SOTdYK2QwR08xV0gyVUFsUGE5TGdWN2tXNjBRClE4Um9Mb2xhWFFweWxSUkFE\n" +
                "a0E9");
        obj.put("businessTypeCode","BASE_ITEM_DTL");
        obj.put("data",list);
        obj.put("rowCnt",list.size());

        try {
            param = param + URLEncoder.encode(obj.toString(), "UTF-8");


            String resultString = postRequest(url, param);
            System.out.println("resultString:" + resultString);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public String postRequest(String url, String param) {
        StringBuffer result = new StringBuffer();
        byte[] data = param.getBytes();
        HttpURLConnection conn = null;
        OutputStream out = null;
        BufferedReader reader = null;
        try {
            URL restUrl = new URL(url);
            conn = (HttpURLConnection) restUrl.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            // 设置不用缓存
            conn.setUseCaches(false);
            conn.setRequestProperty("Content-Length", String.valueOf(data.length));
            conn.setRequestProperty("Charset", "UTF-8");
            // 设置文件类型:
            conn.setRequestProperty("contentType", "application/json");
            conn.connect();
            out = conn.getOutputStream();
            out.write(data);
            out.flush();

            int responseCode = conn.getResponseCode();
            if(responseCode != 200){
                throw new RuntimeException("Error responseCode:" + responseCode);
            }

            String output = null;
            reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
            while((output=reader.readLine()) != null){
                result.append(output);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("调用接口出错：param+"+param);
        } finally {
            try {
                if(reader != null){
                    reader.close();
                }
                if(out != null){
                    out.close();
                }
                if(conn != null){
                    conn.disconnect();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        return result.toString();
    }

}
