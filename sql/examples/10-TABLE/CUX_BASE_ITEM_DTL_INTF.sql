-- drop table CUX.CUX_BASE_ITEM_DTL_INTF

create table CUX.CUX_BASE_ITEM_DTL_INTF
(
  intf_id                        NUMBER,
  intf_batch_id                  NUMBER,
  item_code                      varchar2(100),
  item_desc                      varchar2(500),
  item_type                      varchar2(100),
  creation_date                  DATE,
  created_by                     NUMBER,
  last_update_date               DATE,
  last_updated_by                NUMBER,
  last_update_login              NUMBER,
  attribute_category             VARCHAR2(30),
  attribute1                     VARCHAR2(150),
  attribute2                     VARCHAR2(150),
  attribute3                     VARCHAR2(150),
  attribute4                     VARCHAR2(150),
  attribute5                     VARCHAR2(150),
  attribute6                     VARCHAR2(150),
  attribute7                     VARCHAR2(150),
  attribute8                     VARCHAR2(150),
  attribute9                     VARCHAR2(150),
  attribute10                    VARCHAR2(150),
  attribute11                    VARCHAR2(150),
  attribute12                    VARCHAR2(150),
  attribute13                    VARCHAR2(150),
  attribute14                    VARCHAR2(150),
  attribute15                    VARCHAR2(150),
  source_header_id               VARCHAR2(240),
  SCUX_SOURCE_ID                 VARCHAR2(240),
  intf_attribute1                VARCHAR2(150),
  intf_attribute2                VARCHAR2(150),
  intf_attribute3                VARCHAR2(150),
  intf_attribute4                VARCHAR2(150),
  intf_attribute5                VARCHAR2(150),
  intf_status                    VARCHAR2(30),
  intf_message                   VARCHAR2(4000)

);
-- Add comments to the columns 

comment on column CUX.CUX_BASE_ITEM_DTL_INTF.creation_date
  is '创建日期';
comment on column CUX.CUX_BASE_ITEM_DTL_INTF.created_by
  is '创建人';
comment on column CUX.CUX_BASE_ITEM_DTL_INTF.last_update_date
  is '最后更新日期';
comment on column CUX.CUX_BASE_ITEM_DTL_INTF.last_updated_by
  is '最后更新人';
comment on column CUX.CUX_BASE_ITEM_DTL_INTF.create_by_name
  is '创建者';
comment on column CUX.CUX_BASE_ITEM_DTL_INTF.last_update_by_name
  is '更新者';

-- Create/Recreate indexes 
create index CUX.CUX_BASE_ITEM_DTL_N1 on CUX.CUX_BASE_ITEM_DTL_INTF (INTF_BATCH_ID);
create unique index CUX.CUX_BASE_ITEM_DTL_UK on CUX.CUX_BASE_ITEM_DTL_INTF (INTF_ID);
create public synonym CUX_BASE_ITEM_DTL_INTF for cux.CUX_BASE_ITEM_DTL_INTF
