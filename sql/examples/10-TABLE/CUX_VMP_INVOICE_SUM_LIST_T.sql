--DROP table CUX.cux_Vmp_Invoice_Sum_List_t;
create  table CUX.cux_Vmp_Invoice_Sum_List_t
(
  INTF_ID                  NUMBER,
  intf_batch_id            NUMBER,
  BUSINESS_TYPE_CODE       CHAR(50),
  OU_NAME                  VARCHAR2(240) not null,
  ORG_ID                   NUMBER(15),
  VENDOR_ID                NUMBER(15),
  VENDOR_UNINOVICE_AMT     NUMBER,
  UPAID_SUM_AMT            NUMBER,
  INVOICE_ID               NUMBER,
  INVOICE_NUM              VARCHAR2(200) ,
  INVOICE_AMOUNT           NUMBER,
  INVOICE_DATE             DATE,
  INVOICE_CURRENCY_CODE    VARCHAR2(100) ,
  AMOUNT_PAID              NUMBER,
  INVOICE_UPAID_AMT        NUMBER,
  DUE_DATE                 DATE,
  PAYMENT_STATUS_DISPLAYED VARCHAR2(200),
  CREATION_DATE            DATE
) 
;
CREATE PUBLIC SYNONYM cux_Vmp_Invoice_Sum_List_t FOR CUX.cux_Vmp_Invoice_Sum_List_t;
