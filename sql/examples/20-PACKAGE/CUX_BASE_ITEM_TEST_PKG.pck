CREATE OR REPLACE PACKAGE CUX_BASE_ITEM_TEST_PKG IS

  -- Author  : WE
  -- Created : 2017/4/14 16:11:07
  -- Purpose : 
  TYPE t_Rec IS RECORD(
     Org_Id                     NUMBER
    ,Organization_Id            NUMBER
    ,Organization_Code          VARCHAR2(150)
    ,Transaction_Date           DATE
    ,Trans_Quantity             NUMBER
    ,Inventory_Item_Id          NUMBER
    ,Item_Loc_Control_Code      VARCHAR2(150)
    ,Lot_Control_Flag           VARCHAR2(150)
    ,Serial_Control_Flag        VARCHAR2(150)
    ,Reason_Id                  NUMBER
    ,Transaction_Action_Id      NUMBER
    ,Transaction_Type_Id        NUMBER
    ,Transaction_Source_Type_Id NUMBER
    ,Locator_Type               VARCHAR2(20)
    ,Locator_Id                 NUMBER
    ,Negative_Flag              VARCHAR2(20)
    ,Disposition_Id             NUMBER
    ,Trans_Source_Id            NUMBER
    ,Transfer_Org_Id            NUMBER
    ,Transfer_Organization_Id   NUMBER
    ,Transfer_Organization_Code VARCHAR2(240)
    ,Transfer_Locator_Type      VARCHAR2(240)
    ,Transfer_Subinventory      VARCHAR2(240)
    ,Transfer_Locator_Id        NUMBER
    ,Asset_Inventory1           VARCHAR2(240)
    ,Asset_Inventory2           VARCHAR2(240)
    ,Ship_To_Location_Id        NUMBER
    ,Created_By                 NUMBER
    ,Last_Updated_By            NUMBER
    --
    );
  PROCEDURE Import_Main(Pn_Intf_Batch_Id NUMBER
                       ,Pv_Ret_Status    OUT VARCHAR2
                       ,Pv_Ret_Message   OUT VARCHAR2
                       ,PV_RET_REQUEST_ID OUT NUMBER);

END CUX_BASE_ITEM_TEST_PKG;
/
CREATE OR REPLACE PACKAGE BODY CUX_BASE_ITEM_TEST_PKG IS

  Gv_Package_Name    VARCHAR2(1000) := 'CUX_BASE_ITEM_PKG'; --包名称
  Gv_Ret_Sts_Error   VARCHAR2(10) := Fnd_Api.g_Ret_Sts_Error; --error code
  Gv_Ret_Sts_Success VARCHAR2(10) := Fnd_Api.g_Ret_Sts_Success; --success code
  Gv_Is_Right_Batch  VARCHAR2(10); -- 校验数据是否全部正确

  --接口表数据状态
  Gv_Intf_Success VARCHAR2(10) := 'SUCCESS';
  Gv_Intf_Error   VARCHAR2(10) := 'ERROR';
  Gv_Intf_Running VARCHAR2(10) := 'RUNNING';
  Gv_Intf_Failure VARCHAR2(10) := 'FAILURE';
  Gv_Intf_New     VARCHAR2(10) := 'NEW';

  --更新成功状态
  PROCEDURE Update_Success(Pn_Intf_Id IN NUMBER, Pn_Intf_Batch_Id IN NUMBER) IS
  
  BEGIN
    UPDATE CUX_BASE_ITEM_DTL_INTF Cti
       SET Cti.Intf_Status = Gv_Intf_Success
     WHERE Cti.Intf_Id = Pn_Intf_Id
       AND Cti.Intf_Batch_Id = Pn_Intf_Batch_Id;
  END Update_Success;

  --更新失败状态
  PROCEDURE Update_Error(Pn_Intf_Id IN NUMBER, Pn_Intf_Batch_Id IN NUMBER, Pv_Msg IN VARCHAR2) IS
  
  BEGIN
    UPDATE CUX_BASE_ITEM_DTL_INTF Cti
       SET Cti.Intf_Status = Gv_Intf_Error
          ,Cti.Intf_Message = Pv_Msg
     WHERE Cti.Intf_Id = Pn_Intf_Id
       AND Cti.Intf_Batch_Id = Pn_Intf_Batch_Id;
  END Update_Error;

  /*===============================================================
  Program Name: Update_Interface_Status
  Author : $WE_tjj
  Created : $2017-02-27 $11:47
  Purpose : 更新中间表数据
  Parameters :
  In Pn_Intf_Batch_Id 将导入数据的批ID
  IN Pn_Intf_Line_Id  数据行ID
  Out RETCODE 返回错误代码
  Out ERRBUF 返回错误消息
  Return : 返回值说明
  Description: 当校验通过或者失败调用更新中间表数据程序
  Update History
  Version       Date            Name                  Description
  --------     ----------    ---------------   --------------------
  V1.0         $2017-02-27   $WE_tjj        Creation
  ===============================================================*/
  PROCEDURE Update_Interface_Status(Pn_Intf_Batch_Id NUMBER
                                   ,Pn_Intf_Line_Id  NUMBER
                                   ,Pr_Rec_Data      IN t_Rec
                                   ,Pv_Line_Status   VARCHAR2
                                   ,Pv_Message       VARCHAR2
                                   ,Pv_Ret_Status    OUT VARCHAR2
                                   ,Pv_Ret_Message   OUT VARCHAR2) IS
    Lv_Proce_Name   VARCHAR2(100) := Gv_Package_Name || '.Update_Interface_Status:';
    Lv_Process_Step VARCHAR2(3000);
    Lv_Intf_Status  VARCHAR2(50);
  BEGIN
    --初始化
    Pv_Ret_Status := Fnd_Api.g_Ret_Sts_Success;
  
    Lv_Process_Step := '开始更新中间表';
  
    IF Pv_Line_Status = Fnd_Api.g_Ret_Sts_Success THEN
      Lv_Intf_Status := Gv_Intf_Running;
    ELSE
      Lv_Intf_Status := Gv_Intf_Failure;
    END IF;
  
    UPDATE CUX_BASE_ITEM_DTL_INTF Mmt
       SET Mmt.Intf_Status = Lv_Intf_Status
          ,Mmt.Intf_Message = Pv_Message
       
         
          ,Mmt.Created_By = Pr_Rec_Data.Created_By
          ,Mmt.Last_Updated_By = Pr_Rec_Data.Last_Updated_By
     WHERE Mmt.Intf_Batch_Id = Pn_Intf_Batch_Id
       AND Mmt.Intf_Id = Pn_Intf_Line_Id;
  
  EXCEPTION
    WHEN OTHERS THEN
      Pv_Ret_Status  := Gv_Ret_Sts_Error;
      Pv_Ret_Message := Lv_Proce_Name || Lv_Process_Step || '时发生异常:' || SQLERRM;
  END Update_Interface_Status;
  FUNCTION Get_Disposition_Id(p_Organization_Id IN NUMBER, p_Account_Alias IN VARCHAR) RETURN NUMBER IS
    v_Disposition_Id NUMBER;
  BEGIN
    SELECT Mgd.Disposition_Id
      INTO v_Disposition_Id
      FROM Mtl_Generic_Dispositions Mgd
     WHERE Mgd.Organization_Id = p_Organization_Id
       AND Mgd.Segment1 = p_Account_Alias;
  
    RETURN v_Disposition_Id;
  EXCEPTION
    WHEN No_Data_Found THEN
      RAISE Fnd_Api.g_Exc_Error;
    WHEN Too_Many_Rows THEN
      RAISE Fnd_Api.g_Exc_Error;
    WHEN OTHERS THEN
      RAISE Fnd_Api.g_Exc_Unexpected_Error;
  END Get_Disposition_Id;

  --校验库存组织

  PROCEDURE Validate_Ship_To_Location(Xv_Ret_Status    OUT VARCHAR2
                                     ,Xn_Location_Id   OUT NUMBER
                                     ,Pv_Location_Code IN VARCHAR2) IS
    Ln_Location_Id NUMBER;
  BEGIN
    BEGIN
      SELECT Location_Id
        INTO Ln_Location_Id
        FROM Hr_Locations_All
       WHERE (Location_Code = Pv_Location_Code)
         AND ((Inactive_Date > SYSDATE OR Inactive_Date IS NULL));
    EXCEPTION
      WHEN OTHERS THEN
        Ln_Location_Id := NULL;
    END;
    IF Xn_Location_Id IS NOT NULL THEN
      Xv_Ret_Status := 'E';
    ELSE
      Xv_Ret_Status := 'S';
    END IF;
    Xn_Location_Id := Ln_Location_Id;
  END Validate_Ship_To_Location;
  /*===============================================================
  Program Name: Check_Row
  Author : $WE_tjj
  Created : $2017-02-27 $11:47
  Purpose : 地点导入主程序校验规则
  Parameters :
  In Pn_Intf_Batch_Id 将导入数据的批ID
  Out RETCODE 返回错误代码
  Out ERRBUF 返回错误消息
  Return : 返回值说明
  Description: 校验用户导入的数据
              1 验证值集是否存在
              2 验证是否是从属值集
              3 验证安全性规则
              4 验证规则要素类型
              5 验证规则要素是否存在
  Update History
  Version       Date            Name                  Description
  --------     ----------    ---------------   --------------------
  V1.0         $2017-02-27   $WE_tjj        Creation
  ===============================================================*/
  PROCEDURE Check_Rows(Pn_Intf_Batch_Id IN NUMBER
                      ,Pv_Ret_Status    OUT VARCHAR2
                      ,Pv_Ret_Message   OUT VARCHAR2) IS
    Lv_Proce_Name    VARCHAR2(100) := Gv_Package_Name || '.Check_Row:'; --程序过程名称
    Lv_Process_Step  VARCHAR2(3000); --程序过程名称
    Ln_Intf_Batch_Id NUMBER; -- 批ID
    Ln_Intf_Id       NUMBER; --数据行ID
    Ln_Onhand_Qty    NUMBER; --现有量
    Ln_Avqty         NUMBER; --可用量
    Lv_Line_Status   VARCHAR2(2); --数据行是否错误状态
    Le_Exception EXCEPTION; --异常
    -- Xv_Ret_Status VARCHAR2(10);
    Vr_Rec t_Rec;
  
    Lv_Ret_Status VARCHAR2(10);
    -- Lv_Ret_Message VARCHAR2(4000);
  
    CURSOR Cur_Data(Un_Intf_Batch_Id NUMBER) IS
      SELECT *
        FROM CUX_BASE_ITEM_DTL_INTF Mmt --安全性规则临时表
       WHERE Mmt.Intf_Batch_Id = Un_Intf_Batch_Id -- 限制批次
         AND Mmt.Intf_Status = Gv_Intf_New; --抓取初始状态为NEW的数据进行校验
  
  BEGIN
    --Dbms_Output.Put_Line('ABCD2');
  
    --初始化参数
    Ln_Intf_Batch_Id := Pn_Intf_Batch_Id;
    Pv_Ret_Status    := Fnd_Api.g_Ret_Sts_Success;
  
    --校验程序
    Lv_Process_Step := '校验程序开始';
  
    /*-检验程序开始
    FOR Lr_Data_Rec IN Cur_Data(Ln_Intf_Batch_Id) LOOP
    
      --Dbms_Output.Put_Line('ABCD2');
      --初始化消息
      Scux_Fnd_Message.Init_Msg;
      Lv_Line_Status              := Fnd_Api.g_Ret_Sts_Success; --默认行正确
      Ln_Intf_Id                  := Lr_Data_Rec.Intf_Id;
      Ln_Onhand_Qty               := 0; --现有量
      Ln_Avqty                    := 0; --可用量
      Vr_Rec                      := NULL;
      Lr_Data_Rec.Organization_Id := NULL;
      IF Lr_Data_Rec.Organization_Id IS NULL AND Lr_Data_Rec.Organization_Code IS NULL THEN
      
        Scux_Fnd_Message.Add_Msg('未指定库存组织，库存组织为空，请检查!');
        Lv_Line_Status    := 'E';
        Gv_Is_Right_Batch := 'E';
      ELSE
        Validate_Org(Xv_Ret_Status       => Lv_Ret_Status
                    ,p_Organization_Id   => Lr_Data_Rec.Organization_Id
                    ,p_Organization_Code => Lr_Data_Rec.Organization_Code
                    ,x_Org_Id            => Vr_Rec.Org_Id
                    ,x_Organization_Id   => Vr_Rec.Organization_Id
                    ,x_Organization_Code => Vr_Rec.Organization_Code);
        IF Lv_Ret_Status = 'E' THEN
          Scux_Fnd_Message.Add_Msg('无效的库存组织');
          Lv_Line_Status := 'E';
          --  Gv_Is_Right_Batch := 'E';
        END IF;
      END IF;
    
      --事务日期检验
      IF Lr_Data_Rec.Transaction_Date IS NULL THEN
        Vr_Rec.Transaction_Date := SYSDATE;
      ELSE
        Vr_Rec.Transaction_Date := Lr_Data_Rec.Transaction_Date;
      END IF;
      Validate_Transaction_Date(Xv_Ret_Status      => Lv_Ret_Status
                               ,p_Organization_Id  => Vr_Rec.Organization_Id
                               ,p_Transaction_Date => Vr_Rec.Transaction_Date);
      IF Lv_Ret_Status = 'E' THEN
      
        Scux_Fnd_Message.Add_Msg('请检查事务处理日期[' ||
                                 To_Char(Vr_Rec.Transaction_Date, 'YYYY-MM-DD HH24:MI:SS') ||
                                 ']对应的库存[' || To_Char(Vr_Rec.Organization_Code) || ']会计周期是否打开');
        Lv_Line_Status := 'E';
        -- Gv_Is_Right_Batch := 'E';
      END IF;
      --Dbms_Output.Put_Line('ABCD2');
      
      --更新中间表
      Update_Interface_Status(Pn_Intf_Batch_Id => Ln_Intf_Batch_Id
                             ,Pn_Intf_Line_Id  => Ln_Intf_Id
                             ,Pr_Rec_Data      => Vr_Rec
                             ,Pv_Line_Status   => Lv_Line_Status
                             ,Pv_Message       => Scux_Fnd_Message.Get_All_Msg
                             ,Pv_Ret_Status    => Pv_Ret_Status
                             ,Pv_Ret_Message   => Pv_Ret_Message);
    
    END LOOP;
    --提交验证事务
    COMMIT;*/
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      Pv_Ret_Status  := Gv_Ret_Sts_Error;
      Pv_Ret_Message := Lv_Proce_Name || Lv_Process_Step || '时发生异常:' || SQLERRM;
  END Check_Rows;

  PROCEDURE Import_One_Row(Pn_Intf_Batch_Id NUMBER
                          ,Pn_Intf_Id       NUMBER
                          ,Xv_Ret_Status    OUT VARCHAR2
                          ,Xv_Ret_Message   OUT VARCHAR2) IS
    Lv_Proce_Name   VARCHAR2(100) := Gv_Package_Name || '.Import_One_Row:'; --程序过程名称
    Lv_Process_Step VARCHAR2(3000); --程序过程名称
  
    Ln_Count                    NUMBER;
    Ln_Intf_Id                  NUMBER;
    Lv_Return_Status            VARCHAR2(1);
    Lv_Err_Message              VARCHAR2(2000);
    Ln_Msg_Count                NUMBER;
    Lv_Msg_Data                 VARCHAR2(240);
    Xn_Cash_Receipt_Id          NUMBER;
    Lv_Ret_Status               VARCHAR2(10);
    Lv_Ret_Message              VARCHAR2(4000);
    Lv_Transaction_Iface_Rec    Mtl_Transactions_Interface%ROWTYPE;
    Ln_Transaction_Interface_Id NUMBER;
    Lb_Outcome                  BOOLEAN;
    Ln_Timeout                  NUMBER := 100;
    Lv_Error_Code               VARCHAR2(4000);
    Lv_Error_Explanation        VARCHAR2(4000);
    Le_Exception EXCEPTION;
  
    CURSOR Cur_Data(Un_Intf_Id NUMBER) IS
      SELECT *
        FROM CUX_BASE_ITEM_DTL_INTF Mmt
       WHERE Mmt.Intf_Id = Un_Intf_Id
         AND Mmt.Intf_Batch_Id = Pn_Intf_Batch_Id;
  
  BEGIN
  
    ------------
    --初始化
    ------------
    Xv_Ret_Status   := Fnd_Api.g_Ret_Sts_Success;
    Xv_Ret_Message  := NULL;
    Lv_Process_Step := NULL;
    Ln_Intf_Id      := Pn_Intf_Id;
    --调用标准api或者interface导入标准表
  
    --
  
 /*   FOR Lr_Data_Rec IN Cur_Data(Ln_Intf_Id) LOOP
      SELECT COUNT(0)
        INTO Ln_Count
        FROM Mtl_Material_Transactions Mmt
       WHERE Mmt.Source_Code = Lr_Data_Rec.Source_Code --源代码，随便给
         AND Mmt.Source_Line_Id = Lr_Data_Rec.Source_Line_Id
         AND Mmt.Transaction_Type_Id = Lr_Data_Rec.Transaction_Type_Id;
      IF Ln_Count = 0 THEN
        Lv_Transaction_Iface_Rec.Last_Update_Date    := SYSDATE;
        Lv_Transaction_Iface_Rec.Last_Updated_By     := Nvl(Lr_Data_Rec.Last_Updated_By
                                                           ,Lr_Data_Rec.Created_By);
        Lv_Transaction_Iface_Rec.Creation_Date       := SYSDATE;
        Lv_Transaction_Iface_Rec.Created_By          := Lr_Data_Rec.Created_By;
        Lv_Transaction_Iface_Rec.Last_Update_Login   := -1;
        Lv_Transaction_Iface_Rec.Program_Update_Date := SYSDATE;
        SELECT Mtl_Material_Transactions_s.Nextval
          INTO Ln_Transaction_Interface_Id
          FROM Dual;
      
        --  --Dbms_Output.Put_Line('v_Trans_Source_Id: ' || v_Trans_Source_Id);
        Lv_Transaction_Iface_Rec.Transaction_Header_Id := Ln_Transaction_Interface_Id;
        Lv_Transaction_Iface_Rec.Transaction_Mode      := 3;
        Lv_Transaction_Iface_Rec.Process_Flag          := 1;
        Lv_Transaction_Iface_Rec.Transaction_Type_Id   := Lr_Data_Rec.Transaction_Type_Id; --31; --mtl_transaction_types（事务处理类型）--31 账户别名发放 /41 账户别名接收
        Lv_Transaction_Iface_Rec.Transaction_Source_Id := Lr_Data_Rec.Transaction_Source_Id; --160; --账户/账户别名：transaction_source_id  杂项：distribution_account_id--
        Lv_Transaction_Iface_Rec.Organization_Id       := Lr_Data_Rec.Organization_Id; --库存组织
        Lv_Transaction_Iface_Rec.Inventory_Item_Id     := Lr_Data_Rec.Inventory_Item_Id; --物料id
        Lv_Transaction_Iface_Rec.Subinventory_Code     := Lr_Data_Rec.Subinventory_Code; --子库存
        Lv_Transaction_Iface_Rec.Locator_Id            := Lr_Data_Rec.Locator_Id; --货位
        Lv_Transaction_Iface_Rec.Ship_To_Location_Id   := Lr_Data_Rec.Ship_To_Location_Id; --地点
        Lv_Transaction_Iface_Rec.Reason_Id             := Lr_Data_Rec.Reason_Id; --原因
      
        Lv_Transaction_Iface_Rec.Transaction_Quantity  := Lr_Data_Rec.Transaction_Quantity; --数量   -- + 为接收 / - 为发放
        Lv_Transaction_Iface_Rec.Transaction_Uom       := Lr_Data_Rec.Transaction_Uom; --单位
        Lv_Transaction_Iface_Rec.Transaction_Date      := Lr_Data_Rec.Transaction_Date; --时间
        Lv_Transaction_Iface_Rec.Transfer_Organization := Lr_Data_Rec.Transfer_Organization_Id; --转移组织
        Lv_Transaction_Iface_Rec.Transfer_Subinventory := Lr_Data_Rec.Transfer_Subinventory; --转移子库
        Lv_Transaction_Iface_Rec.Transfer_Locator      := Lr_Data_Rec.Transfer_Locator_Id; --转移货位
        Lv_Transaction_Iface_Rec.Transaction_Cost      := Lr_Data_Rec.Transaction_Value; --事务处理金额
        Lv_Transaction_Iface_Rec.New_Average_Cost      := Lr_Data_Rec.Transaction_Value; --事务处理金额
        Lv_Transaction_Iface_Rec.Attribute_Category    := Lr_Data_Rec.Attribute_Category;
        Lv_Transaction_Iface_Rec.Attribute5            := Lr_Data_Rec.Attribute5;
        Lv_Transaction_Iface_Rec.Attribute6            := Lr_Data_Rec.Attribute6;
      
        --
        BEGIN
          SELECT Pv.Vendor_Id
            INTO Lv_Transaction_Iface_Rec.Attribute7
            FROM Po_Vendors Pv
           WHERE Pv.Segment1 = Lr_Data_Rec.Attribute7;
        EXCEPTION
          WHEN OTHERS THEN
            Lv_Transaction_Iface_Rec.Attribute7 := NULL;
        END;
        --  Lv_Transaction_Iface_Rec.Attribute7 := Lr_Data_Rec.Attribute7;
        --下面三个为必输字段
        Lv_Transaction_Iface_Rec.Source_Code      := Lr_Data_Rec.Source_Code; --源代码，随便给
        Lv_Transaction_Iface_Rec.Source_Header_Id := Lr_Data_Rec.Source_Header_Id; --源行ID ，随便给 --系统使用一般采用唯一标准
        Lv_Transaction_Iface_Rec.Source_Line_Id   := Lr_Data_Rec.Source_Line_Id; --源行ID，随便给 --系统使用一般采用唯一标准
      
        INSERT INTO Inv.Mtl_Transactions_Interface
        VALUES Lv_Transaction_Iface_Rec;
        Ln_Timeout := 100;
        Lb_Outcome := Mtl_Online_Transaction_Pub.Process_Online(p_Transaction_Header_Id => Lv_Transaction_Iface_Rec.Transaction_Header_Id
                                                               ,p_Timeout               => Ln_Timeout
                                                               ,p_Error_Code            => Lv_Error_Code
                                                               ,p_Error_Explanation     => Lv_Error_Explanation);
      
        IF (Lb_Outcome = FALSE) THEN
          Dbms_Output.Put_Line('Failed to process the transaction');
          Dbms_Output.Put_Line('Error code: ' || Lv_Error_Code);
          Dbms_Output.Put_Line('Error message: ' || Lv_Error_Explanation);
        
          DELETE Inv.Mtl_Transactions_Interface
           WHERE Transaction_Header_Id = Lv_Transaction_Iface_Rec.Transaction_Header_Id;
          COMMIT;
          RAISE Le_Exception;
          \*  ELSE
          --Dbms_Output.Put_Line('Transaction with header id ' ||
                               To_Char(Lv_Transaction_Iface_Rec.Transaction_Header_Id) ||
                               ' has been processed successfully');
          COMMIT; --No need here*\
        END IF;
      ELSE
        Xv_Ret_Status := Gv_Ret_Sts_Success;
      END IF;
    
    END LOOP;
  */
    COMMIT;
  EXCEPTION
    WHEN Le_Exception THEN
      ROLLBACK;
      Xv_Ret_Status  := Fnd_Api.g_Ret_Sts_Error;
      Xv_Ret_Message := Lv_Proce_Name || Lv_Process_Step || '时发生异常:' || Lv_Error_Explanation;
    WHEN OTHERS THEN
      ROLLBACK;
      Xv_Ret_Status  := Fnd_Api.g_Ret_Sts_Unexp_Error;
      Xv_Ret_Message := Lv_Proce_Name || Lv_Process_Step || '时发生未预期异常:' || SQLERRM;
  END Import_One_Row;

  /*===============================================================
  Program Name: Import_Rows
  Author : $WE_tjj
  Created : $2017-02-27 $11:47
  Purpose : 地点导入程序
  Parameters :
  In Pn_Intf_Batch_Id 将导入数据的批ID
  Out RETCODE 返回错误代码
  Out ERRBUF 返回错误消息
  Return : 返回值说明
  Description:  地点导入程序主要围绕下列五个数据库对象进行维护
  Update History
  Version       Date            Name                  Description
  --------     ----------    ---------------   --------------------
  V1.0         $2017-02-27   $WE_tjj        Creation
  ===============================================================*/
  PROCEDURE Import_Rows(Pn_Intf_Batch_Id NUMBER
                       ,Pv_Ret_Status    OUT VARCHAR2
                       ,Pv_Ret_Message   OUT VARCHAR2) IS
    Lv_Proce_Name   VARCHAR2(100) := Gv_Package_Name || '.Import_Row:'; --程序过程名称
    Lv_Process_Step VARCHAR2(3000); --程序过程名称
  
    Ln_Intf_Batch_Id NUMBER; --接口批ID
  
    l_Ret_Status VARCHAR2(100);
    l_Ret_Msg    VARCHAR2(2000);
    CURSOR Cur_Rec(Un_Intf_Batch_Id NUMBER) IS
      SELECT *
        FROM CUX_BASE_ITEM_DTL_INTF Mmt
       WHERE Mmt.Intf_Batch_Id = Un_Intf_Batch_Id -- 限制批次
 
         AND Mmt.Intf_Status = Gv_Intf_Running --校验通过
      ;
    Ln_Application_Id    NUMBER;
    Ln_Org_Id            NUMBER;
    Ln_Responsibility_Id NUMBER;
    Ln_User_Id           NUMBER;
    Le_Exception EXCEPTION;
  BEGIN
    ------------
    --初始化变量
    ------------
    --Dbms_Output.Put_Line('A');
    Pv_Ret_Status    := Gv_Ret_Sts_Success;
    Pv_Ret_Message   := NULL;
    Ln_Intf_Batch_Id := Pn_Intf_Batch_Id;
  
    ------------
    --初始化职责
    ------------
    Lv_Process_Step := '初始化职责';
    BEGIN
     NULL;
      /*SELECT DISTINCT Frv.Responsibility_Id
                     ,Fav.Application_Id
                     ,Hou.Organization_Id
                     ,Smmt.Created_By
        INTO Ln_Responsibility_Id
            ,Ln_Application_Id
            ,Ln_Org_Id
            ,Ln_User_Id
        FROM Fnd_Application_Vl           Fav
            ,Fnd_Responsibility_Vl        Frv
            ,Org_Organization_Definitions Ood
            ,Hr_Operating_Units           Hou
            ,CUX_BASE_ITEM_DTL_INTF    Smmt
       WHERE Fav.Application_Name = '库存管理系统'
         AND Frv.Responsibility_Name LIKE Hou.Short_Code || '%库存管理员'
         AND Frv.Application_Id = Fav.Application_Id
         AND Ood.Operating_Unit = Hou.Organization_Id
         AND Ood.Organization_Id = Smmt.Organization_Id --REC1.ORG_ID
         AND Smmt.Intf_Batch_Id = Pn_Intf_Batch_Id;*/
    EXCEPTION
      WHEN OTHERS THEN
        Pv_Ret_Message := '获取初始化职责错误，请联系系统管理员';
    END;
  
    --  
    Fnd_Global.Apps_Initialize(User_Id      => Ln_User_Id
                              ,Resp_Id      => Ln_Responsibility_Id
                              ,Resp_Appl_Id => Ln_Application_Id); --应付帐款应用ID
    Mo_Global.Set_Policy_Context('S', Ln_Org_Id); --261是OU的ID
    ------------
    --主程序开始
    ------------
    Lv_Process_Step := '数据导入';
    FOR Lr_Data_Rec IN Cur_Rec(Ln_Intf_Batch_Id) LOOP
      --Dbms_Output.Put_Line('AB');
      ------------
      --重置变量
      ------------
      l_Ret_Status := NULL;
      l_Ret_Msg    := NULL;
      --导入数据
      Import_One_Row(Pn_Intf_Batch_Id => Pn_Intf_Batch_Id
                    ,Pn_Intf_Id       => Lr_Data_Rec.Intf_Id
                    ,Xv_Ret_Status    => l_Ret_Status
                    ,Xv_Ret_Message   => l_Ret_Msg);
      --更新接口记录信息
      IF l_Ret_Status = Fnd_Api.g_Ret_Sts_Success THEN
        Update_Success(Lr_Data_Rec.Intf_Id, Lr_Data_Rec.Intf_Batch_Id);
      ELSE
        Update_Error(Lr_Data_Rec.Intf_Id, Lr_Data_Rec.Intf_Batch_Id, l_Ret_Msg);
      END IF;
      --commit;
    --Dbms_Output.Put_Line('B');
    END LOOP;
  
  EXCEPTION
    WHEN Le_Exception THEN
      ROLLBACK;
      Pv_Ret_Status  := Gv_Ret_Sts_Error;
      Pv_Ret_Message := Lv_Proce_Name || Lv_Process_Step || '时发生异常:' || Pv_Ret_Message;
    WHEN OTHERS THEN
      Pv_Ret_Status  := Gv_Ret_Sts_Error;
      Pv_Ret_Message := '发生异常:' || SQLERRM;
  END Import_Rows;

  /*===============================================================
  Program Name: Main
  Author : $WE_lzf
  Created : $2017-04-27 $11:47
  Purpose : 导入程序主程序
  Parameters :
  In Pn_Intf_Batch_Id 将导入数据的批ID
  Out RETCODE 返回错误代码
  Out ERRBUF 返回错误消息
  Return : 返回值说明
  Description:
  Update History
  Version       Date            Name                  Description
  --------     ----------    ---------------   --------------------
  V1.0         $2017-02-27   $WE_tjj        Creation
  ===============================================================*/
  PROCEDURE Import_Main(Pn_Intf_Batch_Id NUMBER
                       ,Pv_Ret_Status    OUT VARCHAR2
                       ,Pv_Ret_Message   OUT VARCHAR2
                       ,PV_RET_REQUEST_ID OUT NUMBER) IS
    Lv_Proce_Name    VARCHAR2(100) := Gv_Package_Name || '.Main:'; --程序过程名称
    Lv_Process_Step  VARCHAR2(3000); --程序过程名称
    Ln_Intf_Batch_Id NUMBER;
    Lv_Return_Code   VARCHAR2(10);
  
    Le_Exception EXCEPTION;
    v_Sql VARCHAR2(1000);
  BEGIN
  PV_RET_REQUEST_ID:=-10000;
  
    BEGIN
      v_Sql := 'ALTER SESSION set NLS_LANGUAGE =' || '''SIMPLIFIED CHINESE''';
      EXECUTE IMMEDIATE v_Sql;
    EXCEPTION
      WHEN OTHERS THEN
        Fnd_File.Put_Line(Fnd_File.Log, 'error :语言改变失败！');
    END;
    
    INSERT INTO Cux_Track_Log_t
      (Tt_Type
      ,Tt_Seq
      ,Tt_Content
      ,Creation_Date
      ,REF1
      ,REF2)
    VALUES
      ('TEST'
      ,1000
      ,Pn_Intf_Batch_Id
      ,SYSDATE
      ,Gv_Package_Name
      ,'START');
      COMMIT;
      Pv_Ret_Message:='执行成功';
      Pv_Ret_Status:='SUCCESS';
    
  /*  --初始化参数
    Pv_Ret_Status     := Gv_Ret_Sts_Success;
    Pv_Ret_Message    := NULL;
    Ln_Intf_Batch_Id  := Pn_Intf_Batch_Id;
    Gv_Is_Right_Batch := 'S';
  
    Lv_Process_Step := '调用Check_Row';
    --执行校验接口表数据记录
    Check_Rows(Pn_Intf_Batch_Id => Ln_Intf_Batch_Id
              ,Pv_Ret_Status    => Lv_Return_Code
              ,Pv_Ret_Message   => Pv_Ret_Message);
  
    IF Lv_Return_Code <> Gv_Ret_Sts_Success THEN
      RAISE Le_Exception;
    ELSIF Gv_Is_Right_Batch = Gv_Ret_Sts_Success THEN
      --如果批次数据全部校验成功,则执行导入，否则导入程序结束
      ----Import_Row
      Lv_Process_Step := '调用Import_Row';
      Import_Rows(Pn_Intf_Batch_Id => Ln_Intf_Batch_Id
                 ,Pv_Ret_Status    => Lv_Return_Code
                 ,Pv_Ret_Message   => Pv_Ret_Message);
      IF Lv_Return_Code <> Gv_Ret_Sts_Success THEN
        RAISE Le_Exception;
      END IF;
    END IF;*/
    
    
  EXCEPTION
    WHEN Le_Exception THEN
      Pv_Ret_Status  := Gv_Ret_Sts_Error;
      Pv_Ret_Message := Lv_Proce_Name || Lv_Process_Step || '时发生异常:' || SQLERRM;
    WHEN OTHERS THEN
      ROLLBACK;
      Pv_Ret_Status  := Gv_Ret_Sts_Error;
      Pv_Ret_Message := Lv_Proce_Name || Lv_Process_Step || '时发生未知异常:' || SQLERRM;
  END Import_Main;

END CUX_BASE_ITEM_TEST_PKG;
/
