create public synonym CUX_WS_INTF_T for cux.CUX_WS_INTF_T;
create public synonym CUX_ws_intf_revoke_t for cux.CUX_ws_intf_revoke_t;
create public synonym CUX_ERP_EEEE_T for cux.CUX_ERP_EEEE_T;
create public synonym CUX_ERP_TEST_T for cux.CUX_ERP_TEST_T;
create public synonym CUX_GET_DOCNO_T for cux.CUX_GET_DOCNO_T;
create public synonym CUX_WS_INTF_LOG_DTL_T for cux.CUX_WS_INTF_LOG_DTL_T;
create public synonym CUX_WS_INTF_LOG_T for cux.CUX_WS_INTF_LOG_T;
