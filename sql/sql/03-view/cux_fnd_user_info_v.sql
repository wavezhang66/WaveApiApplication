create or replace view cux_fnd_user_info_v as
SELECT usr.user_name
      ,usr.user_id
      ,get_pwd.decrypt((SELECT (SELECT get_pwd.decrypt(fnd_web_sec.get_guest_username_pwd,
                                                      usertable.encrypted_foundation_password)
                               FROM   dual) AS apps_password
                       FROM   apps.fnd_user usertable
                       WHERE  usertable.user_name =
                              (SELECT substr(fnd_web_sec.get_guest_username_pwd,
                                             1,
                                             instr(fnd_web_sec.get_guest_username_pwd,
                                                   '/') - 1)
                               FROM   dual)),
                       usr.encrypted_user_password) passwords
FROM   apps.fnd_user usr
WHERE  1 = 1
AND    usr.start_date <= SYSDATE
AND    nvl(usr.end_date, SYSDATE) >= SYSDATE
