-- Create table
create table CUX.CUX_ERP_TEST_T
(
  erp_test_id       NUMBER not null,
  erp_test_name     VARCHAR2(200),
  batch_id          VARCHAR2(64),
  comments          VARCHAR2(200),
  status            VARCHAR2(20),
  last_update_date  DATE,
  last_updated_by   NUMBER(15),
  creation_date     DATE,
  created_by        NUMBER(15),
  last_update_login NUMBER(15),
  attribute1        VARCHAR2(30),
  attribute2        VARCHAR2(30),
  attribute3        VARCHAR2(30),
  attribute4        VARCHAR2(30),
  attribute5        VARCHAR2(30),
  attribute6        VARCHAR2(30),
  attribute7        VARCHAR2(30),
  attribute8        VARCHAR2(30),
  attribute9        VARCHAR2(30),
  attribute10       VARCHAR2(30),
  attribute11       VARCHAR2(30),
  attribute12       VARCHAR2(30),
  attribute13       VARCHAR2(30),
  attribute14       VARCHAR2(30),
  attribute15       VARCHAR2(30),
  request_id        NUMBER
)
tablespace CUX_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CUX.CUX_ERP_TEST_T
  add primary key (ERP_TEST_ID)
  using index 
  tablespace CUX_DATA
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create sequence 
create sequence CUX.CUX_ERP_TEST_S
minvalue 1
maxvalue 9999999999999999999999999999
start with 10056
increment by 1
nocache;
alter table CUX.CUX_ERP_TEST_T add business_key_id  number;

comment on column CUX.CUX_ERP_TEST_T.business_key_id
  is '业务系统来源主键id';
