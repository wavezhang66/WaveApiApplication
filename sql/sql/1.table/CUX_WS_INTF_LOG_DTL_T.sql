-- drop table CUX.CUX_WS_INTF_LOG_DTL_T
create table CUX.CUX_WS_INTF_LOG_DTL_T
(
  batch_id          VARCHAR2(64),
  line_id           NUMBER,
  step_code         VARCHAR2(30),
  step_status       VARCHAR2(30),
  step_status_desc  VARCHAR2(200),
  detail_msg        VARCHAR2(3500),
  start_date        DATE,
  end_date          DATE,
  comments          VARCHAR2(200),
  last_update_date  DATE,
  last_updated_by   NUMBER(15),
  creation_date     DATE,
  created_by        NUMBER(15),
  last_update_login NUMBER(15),
  attribute1        VARCHAR2(30),
  attribute2        VARCHAR2(30),
  attribute3        VARCHAR2(30),
  attribute4        VARCHAR2(30),
  attribute5        VARCHAR2(30),
  attribute6        VARCHAR2(30),
  attribute7        VARCHAR2(30),
  attribute8        VARCHAR2(30),
  attribute9        VARCHAR2(30),
  attribute10       VARCHAR2(30),
  attribute11       VARCHAR2(30),
  attribute12       VARCHAR2(30),
  attribute13       VARCHAR2(30),
  attribute14       VARCHAR2(30),
  attribute15       VARCHAR2(30),
  request_id        NUMBER
)
tablespace CUX_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CUX.CUX_WS_INTF_LOG_DTL_T.batch_id
  is '接口调用批ID';
comment on column CUX.CUX_WS_INTF_LOG_DTL_T.line_id
  is '日志明细行ID';
comment on column CUX.CUX_WS_INTF_LOG_DTL_T.step_code
  is '节点代码';
comment on column CUX.CUX_WS_INTF_LOG_DTL_T.step_status
  is '节点状态';
comment on column CUX.CUX_WS_INTF_LOG_DTL_T.step_status_desc
  is '节点状态说明';
comment on column CUX.CUX_WS_INTF_LOG_DTL_T.detail_msg
  is '详细信息';
comment on column CUX.CUX_WS_INTF_LOG_DTL_T.start_date
  is '运行开始日期';
comment on column CUX.CUX_WS_INTF_LOG_DTL_T.end_date
  is '运行线束日期';
comment on column CUX.CUX_WS_INTF_LOG_DTL_T.comments
  is '备注';
-- Create sequence 
create sequence CUX.CUX_WS_INTF_LOG_DTL_T_S
minvalue 1
maxvalue 9999999999999999999999999999
start with 10133
increment by 1
nocache;
