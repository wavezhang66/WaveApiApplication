-- drop table CUX.CUX_WS_INTF_LOG_T
create table CUX.CUX_WS_INTF_LOG_T
(
  intf_id           NUMBER,
  batch_id          VARCHAR2(64),
  intf_name         VARCHAR2(200),
  intf_code         VARCHAR2(100),
  intf_message      CLOB,
  intf_status       VARCHAR2(30),
  intf_status_desc  VARCHAR2(200),
  intf_return_msg   VARCHAR2(3000),
  row_count         NUMBER,
  run_date          DATE,
  last_run_date     DATE,
  comments          VARCHAR2(200),
  last_update_date  DATE,
  last_updated_by   NUMBER(15),
  creation_date     DATE,
  created_by        NUMBER(15),
  last_update_login NUMBER(15),
  attribute1        VARCHAR2(30),
  attribute2        VARCHAR2(30),
  attribute3        VARCHAR2(30),
  attribute4        VARCHAR2(30),
  attribute5        VARCHAR2(30),
  attribute6        VARCHAR2(30),
  attribute7        VARCHAR2(30),
  attribute8        VARCHAR2(30),
  attribute9        VARCHAR2(30),
  attribute10       VARCHAR2(30),
  attribute11       VARCHAR2(30),
  attribute12       VARCHAR2(30),
  attribute13       VARCHAR2(30),
  attribute14       VARCHAR2(30),
  attribute15       VARCHAR2(30),
  request_id        NUMBER
)
tablespace CUX_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CUX.CUX_WS_INTF_LOG_T.intf_id
  is '接口后台ID';
comment on column CUX.CUX_WS_INTF_LOG_T.batch_id
  is '接口调用批ID';
comment on column CUX.CUX_WS_INTF_LOG_T.intf_name
  is '接口名称';
comment on column CUX.CUX_WS_INTF_LOG_T.intf_code
  is '接口编码';
comment on column CUX.CUX_WS_INTF_LOG_T.intf_message
  is '接口JSON报文';
comment on column CUX.CUX_WS_INTF_LOG_T.intf_status
  is '接口状态';
comment on column CUX.CUX_WS_INTF_LOG_T.intf_status_desc
  is '接口状态说明';
comment on column CUX.CUX_WS_INTF_LOG_T.intf_return_msg
  is '接口返回信息';
comment on column CUX.CUX_WS_INTF_LOG_T.row_count
  is '接口数据行数';
comment on column CUX.CUX_WS_INTF_LOG_T.run_date
  is '本次运行日期';
comment on column CUX.CUX_WS_INTF_LOG_T.last_run_date
  is '上次运行日期';
comment on column CUX.CUX_WS_INTF_LOG_T.comments
  is '备注';
-- Create sequence 
create sequence CUX.CUX_WS_INTF_LOG_S
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
nocache;
alter table CUX.CUX_WS_INTF_LOG_T add step_code VARCHAR2(30);

comment on column CUX.CUX_WS_INTF_LOG_T.step_code
  is '节点代码';
alter table CUX.CUX_WS_INTF_LOG_T add STEP_MESSAGE  CLOB;

comment on column CUX.CUX_WS_INTF_LOG_T.STEP_MESSAGE
  is '节点操作所需的数据';
