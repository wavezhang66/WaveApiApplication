-- Create table
create table CUX.CUX_WS_INTF_REVOKE_T
(
  intf_id           NUMBER,
  repeat_time       NUMBER,
  repeat_unit       VARCHAR2(30),
  repeat_point      VARCHAR2(200),
  comments          VARCHAR2(200),
  last_update_date  DATE,
  last_updated_by   NUMBER(15),
  creation_date     DATE,
  created_by        NUMBER(15),
  last_update_login NUMBER(15),
  attribute1        VARCHAR2(30),
  attribute2        VARCHAR2(30),
  attribute3        VARCHAR2(30),
  attribute4        VARCHAR2(30),
  attribute5        VARCHAR2(30),
  attribute6        VARCHAR2(30),
  attribute7        VARCHAR2(30),
  attribute8        VARCHAR2(30),
  attribute9        VARCHAR2(30),
  attribute10       VARCHAR2(30),
  attribute11       VARCHAR2(30),
  attribute12       VARCHAR2(30),
  attribute13       VARCHAR2(30),
  attribute14       VARCHAR2(30),
  attribute15       VARCHAR2(30),
  invoke_id         NUMBER,
  status            VARCHAR2(20)
)
tablespace CUX_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CUX.CUX_WS_INTF_REVOKE_T.intf_id
  is '接口后台ID';
comment on column CUX.CUX_WS_INTF_REVOKE_T.repeat_time
  is '重复运行时间';
comment on column CUX.CUX_WS_INTF_REVOKE_T.repeat_unit
  is '时间单位';
comment on column CUX.CUX_WS_INTF_REVOKE_T.repeat_point
  is '重复运行时点';
comment on column CUX.CUX_WS_INTF_REVOKE_T.comments
  is '备注';
comment on column CUX.CUX_WS_INTF_REVOKE_T.invoke_id
  is '调度定义表后台ID';
comment on column CUX.CUX_WS_INTF_REVOKE_T.status
  is '接口状态,有效:空;无效:INACTIVE(删除接口)';

-- Create sequence 
create sequence CUX.CUX_WS_INTF_REVOKE_S
minvalue 1
maxvalue 9999999999999999999999999999
start with 28
increment by 1
nocache;
