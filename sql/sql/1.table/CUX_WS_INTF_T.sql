-- Create table
create table CUX.CUX_WS_INTF_T
(
  intf_id            NUMBER,
  intf_name          VARCHAR2(60),
  intf_desc          VARCHAR2(200),
  erp_intf_table     VARCHAR2(200),
  erp_intf_function  VARCHAR2(200),
  business_type_code VARCHAR2(60),
  debug_flag         VARCHAR2(1),
  PAGE_SIZE  NUMBER,
  IS_RSA_FLAG VARCHAR2(100),
  INTERFACE_TYPE_CODE VARCHAR2(100),
  comments           VARCHAR2(200),
  last_update_date   DATE,
  last_updated_by    NUMBER(15),
  creation_date      DATE,
  created_by         NUMBER(15),
  last_update_login  NUMBER(15),
  attribute1         VARCHAR2(30),
  attribute2         VARCHAR2(30),
  attribute3         VARCHAR2(30),
  attribute4         VARCHAR2(30),
  attribute5         VARCHAR2(30),
  attribute6         VARCHAR2(30),
  attribute7         VARCHAR2(30),
  attribute8         VARCHAR2(30),
  attribute9         VARCHAR2(30),
  attribute10        VARCHAR2(30),
  attribute11        VARCHAR2(30),
  attribute12        VARCHAR2(30),
  attribute13        VARCHAR2(30),
  attribute14        VARCHAR2(30),
  attribute15        VARCHAR2(30),
  out_sys_ws         VARCHAR2(200)
)
tablespace APPS_TS_TX_DATA
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 128K
    minextents 1
    maxextents unlimited
    pctincrease 0
  );
-- Add comments to the columns 
comment on column CUX.CUX_WS_INTF_T.intf_id
  is '后台ID';
comment on column CUX.CUX_WS_INTF_T.intf_name
  is '接口名称';
comment on column CUX.CUX_WS_INTF_T.intf_desc
  is '接口说明';
comment on column CUX.CUX_WS_INTF_T.erp_intf_table
  is 'ERP客户化接口表名';
comment on column CUX.CUX_WS_INTF_T.erp_intf_function
  is 'ERP接口处理过程包';
comment on column CUX.CUX_WS_INTF_T.business_type_code
  is '接口业务标识 ';
comment on column CUX.CUX_WS_INTF_T.debug_flag
  is 'DEBUG标识';
comment on column CUX.CUX_WS_INTF_T.comments
  is '备注';
comment on column CUX.CUX_WS_INTF_T.PAGE_SIZE
  is '每页记录';
comment on column CUX.CUX_WS_INTF_T.INTERFACE_TYPE_CODE
  is '接口类型：接收，传送';
comment on column CUX.CUX_WS_INTF_T.IS_RSA_FLAG
  is '数据是否加密';
comment on column CUX.CUX_WS_INTF_T.out_sys_ws
  is '外部系统获取数据同步结果的WS地址';

-- Create sequence 
create sequence CUX.CUX_WS_INTF_S
minvalue 1
maxvalue 9999999999999999999999999999
start with 64
increment by 1
nocache;
alter table CUX.CUX_WS_INTF_T add source_system  VARCHAR2(30);

comment on column CUX.CUX_WS_INTF_T.source_system
  is '来源系统';
