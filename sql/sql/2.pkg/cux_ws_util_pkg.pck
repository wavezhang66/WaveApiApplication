CREATE OR REPLACE PACKAGE CUX_Ws_Util_Pkg IS
  /*===============================================================
  *   COPYRIGHT (C) JB CONSULTING CO., LTD ALL RIGHTS RESERVED
  * ===============================================================
  
  *    PROGRAM NAME:CUX_ws_util_pkg
  *    AUTHOR      :WAVE
  *    DATE        :2019-01-08
  *    PURPOSE     :WAVE WS平台公共方法包
  *    PARAMETERS  :
  *    UPDATE HISTORY
  *    VERSION    DATE         NAME            DESCRIPTION
  *    --------  ----------  ---------------  --------------------
  *     V1.0     2019-01-08     WAVE           CREATION
  *
    ===============================================================*/

  /*===============================================================
  *    PROGRAM NAME:gen_batch_id
  *    AUTHOR      :WAVE
  *    DATE        :2019-01-08
  *    PURPOSE     :产生batch_id(WS2019010800001)
  *    PARAMETERS  :
  *    UPDATE HISTORY
  *    VERSION    DATE         NAME            DESCRIPTION
  *    --------  ----------  ---------------  --------------------
  *     V1.0     2019-01-08     WAVE           CREATION
  *
    ===============================================================*/

  FUNCTION Gen_Batch_Id(p_Pre_Fix IN VARCHAR2 DEFAULT 'WS' --前缀,
                       ,p_Len     IN NUMBER DEFAULT 5 --流水号的位数,
                        ) RETURN VARCHAR2;

  /*===============================================================
  *    PROGRAM NAME:EXE_PACKAGE
  *    AUTHOR      :WAVE
  *    DATE        :2021-03-05
  *    PURPOSE     :动态执行包
  *    PARAMETERS  :
  *    UPDATE HISTORY
  *    VERSION    DATE         NAME            DESCRIPTION
  *    --------  ----------  ---------------  --------------------
  *     V1.0     2021-03-05     WAVE           CREATION
  *
    ===============================================================*/
  PROCEDURE Exe_Package(Pv_Pkg_Name  IN VARCHAR2                       
                       ,PV_Query_Condition     IN VARCHAR2
                       ,Pv_User_Name IN VARCHAR2
                       ,Ret_Status   OUT VARCHAR2
                       ,Ret_Messate  OUT VARCHAR2) ;

END CUX_Ws_Util_Pkg;
/
CREATE OR REPLACE PACKAGE BODY CUX_Ws_Util_Pkg IS
  /*===============================================================
  *   COPYRIGHT (C) JB CONSULTING CO., LTD ALL RIGHTS RESERVED
  * ===============================================================
  
  *    PROGRAM NAME:CUX_ws_util_pkg
  *    AUTHOR      :WAVE
  *    DATE        :2019-01-08
  *    PURPOSE     :WAVE WS平台公共方法包
  *    PARAMETERS  :
  *    UPDATE HISTORY
  *    VERSION    DATE         NAME            DESCRIPTION
  *    --------  ----------  ---------------  --------------------
  *     V1.0     2019-01-08     WAVE           CREATION
  *
    ===============================================================*/

  FUNCTION Gen_Batch_Id(p_Pre_Fix IN VARCHAR2 DEFAULT 'WS' --前缀,
                       ,p_Len     IN NUMBER DEFAULT 5 --流水号的位数,
                        ) RETURN VARCHAR2 IS
    v_Curr_Seq     NUMBER;
    x_Next_Seq     NUMBER;
    v_Date_Code    VARCHAR2(8);
    x_Batch_Number VARCHAR2(80);
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    SELECT To_Char(SYSDATE
                  ,'YYYYMMDD')
      INTO v_Date_Code
      FROM Dual;
  
    BEGIN
    
      SELECT Vus.Next_Sequence
        INTO v_Curr_Seq
        FROM Cux.CUX_Get_Docno_t Vus
       WHERE Vus.Pre_Fix = p_Pre_Fix --'WS'
         AND Vus.Date_Code = v_Date_Code --'20190107'
         FOR UPDATE;
    
      x_Next_Seq := v_Curr_Seq + 1;
      UPDATE Cux.CUX_Get_Docno_t n
         SET n.Next_Sequence = x_Next_Seq
       WHERE n.Pre_Fix = p_Pre_Fix -- 'WS'
         AND n.Date_Code = v_Date_Code; -- '20190107';
    EXCEPTION
      WHEN No_Data_Found THEN
        BEGIN
          INSERT INTO Cux.CUX_Get_Docno_t
            (Pre_Fix
            ,Date_Code
            ,Next_Sequence)
          VALUES
            (p_Pre_Fix -- 'WS'
            ,v_Date_Code
            ,1);
          x_Next_Seq := 1;
        EXCEPTION
          WHEN OTHERS THEN
            IF SQLCODE = -00001 THEN
              SELECT Vus.Next_Sequence
                INTO v_Curr_Seq
                FROM Cux.CUX_Get_Docno_t Vus
               WHERE Vus.Pre_Fix = p_Pre_Fix --'WS'
                 AND Vus.Date_Code = v_Date_Code --'20190107'
                 FOR UPDATE;
            
              x_Next_Seq := v_Curr_Seq + 1;
              UPDATE Cux.CUX_Get_Docno_t n
                 SET n.Next_Sequence = x_Next_Seq
               WHERE n.Pre_Fix = p_Pre_Fix --'WS'
                 AND n.Date_Code = v_Date_Code; --'20190107';
            ELSE
              ROLLBACK;
              RETURN NULL;
            END IF;
        END;
    END;
  
    x_Batch_Number := p_Pre_Fix || v_Date_Code ||
                      Lpad(To_Char(x_Next_Seq)
                          ,p_Len
                          ,'0');
  
    COMMIT;
    RETURN x_Batch_Number;
  END;
  ---------------------------------------------------------------------------------------------------------

  PROCEDURE Exe_Package(Pv_Pkg_Name  IN VARCHAR2                       
                       ,PV_Query_Condition     IN VARCHAR2
                       ,Pv_User_Name IN VARCHAR2
                       ,Ret_Status   OUT VARCHAR2
                       ,Ret_Messate  OUT VARCHAR2) IS
    Lv_Pkg_Name VARCHAR2(100);
    Ln_Bth_Seq  NUMBER;
    Lv_Query_Condition VARCHAR2(3000);
    Lv_Exe_Sql         VARCHAR2(3000);
  
  BEGIN
    Lv_Pkg_Name := Pv_Pkg_Name;
    Lv_Query_Condition:=PV_Query_Condition;
    ---如果视图数据比较复杂,查询比较慢时,可以用包的去插到临时表
    IF Lv_Pkg_Name IS NOT NULL THEN
      SELECT CUX_FND_FILE_IMP_INTF_BATCH_S.NEXTVAL
        INTO Ln_Bth_Seq
        FROM Dual;
      Lv_Query_Condition := NULL;
      COMMIT;
      IF Instr(Upper(Pv_Query_Condition)
              ,' ORDER ') > 0 THEN
        Lv_Query_Condition := Substr(Upper(Pv_Query_Condition)
                                           ,1
                                           ,Instr(Upper(Pv_Query_Condition)
                                                 ,' ORDER '));
        /*Gv_Order_By               := Substr(Upper(Pv_Query_Condition)
                                           ,Instr(Upper(Pv_Query_Condition)
                                                 ,' ORDER '));*/
      ELSE
        Lv_Query_Condition := Pv_Query_Condition;
      END IF;
      BEGIN
      -- Lv_Query_Condition := ' and intf_batch_id=' || Ln_Bth_Seq;
      Lv_Exe_Sql      := 'BEGIN ' || Lv_Pkg_Name || '(Pn_Intf_Batch_Id=>' ||
                         Ln_Bth_Seq || ',' || Lv_Query_Condition ||
                         ',Xv_Ret_Status=>:Xv_Ret_Status,Xv_Ret_Message=>:Xv_Ret_Message) ;END;';
     -- Gv_Process_Step := Lv_Exe_Sql;
      EXECUTE IMMEDIATE Lv_Exe_Sql
        USING OUT Ret_Status, OUT Ret_Messate;
      EXCEPTION WHEN OTHERS THEN
        Ret_Status:='ERROR';
       Ret_Messate:='执行 动态包报错'||Lv_Exe_Sql;
      END;
    ELSE
       Ret_Status:='ERROR';
       Ret_Messate:='包不能为空';
    END IF;
  
  END Exe_Package;

END CUX_Ws_Util_Pkg;
/
